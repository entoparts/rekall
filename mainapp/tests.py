from django.test import TestCase
from django.db.utils import IntegrityError
from .models import User

# Create your tests here.
class UserModelTests(TestCase):

    def test_create_user_expected(self):
        some_user = User()
        some_user.username = 'some user'
        some_user.save()
        self.assertEquals(some_user.username, 'some user')

    def test_unique_users_expected(self):
        some_user = User()
        some_user.username = 'some user'
        some_user.save()
        self.assertEquals(some_user.username, 'some user')
        another_user = User()
        another_user.username = 'some user'
        with self.assertRaises(IntegrityError):
            another_user.save()