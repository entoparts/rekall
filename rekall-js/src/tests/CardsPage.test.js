import React from 'react';
import { render, screen } from '@testing-library/react';
import CardsPage from '../CardsPage';

let sampleProps = {
    page: 'cards',
    targetId: '11',
}

test('renders app', async () => {
  render(<CardsPage {...sampleProps} />);
  const buttonElement = screen.getByText(/Create Card/i);
  expect(buttonElement).toBeInTheDocument();
  const headerElement2 = screen.getByText(/Deck:/i);
  expect(headerElement2).toBeInTheDocument();
});
