import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders app', () => {
  render(<App />);
  const headerElement = screen.getByText(/Rekall/i);
  expect(headerElement).toBeInTheDocument();
  const buttonElement = screen.getByText(/Create Deck/i);
  expect(buttonElement).toBeInTheDocument();
  const headerElement2 = screen.getByText(/Decks/i);
  expect(headerElement2).toBeInTheDocument();
});
