import RecallService from './api/recallservice';
import DeckService from './api/deckservice';

import { React, useState, useEffect } from 'react';
import { TimerComponent } from "./Timer";
import { Card, Button, Descriptions } from 'antd';
import { HomeOutlined } from '@ant-design/icons';


function RecallTile(props) {

  const [isFront, setIsFront] = useState(true);
  const { setIsTicking, seconds, setSeconds } = TimerComponent();

  useEffect(() => {
    setIsTicking(true);
  }, [setIsTicking]);  

  function handleClick(score){
    let duration = seconds.toFixed(1);
    RecallService.update(props.recallId, {score, duration})
    .then(function(response) {
      console.log(response)
      setSeconds(0);
      props.setrIndex(props.rIndex+1)
    })
    .catch(function (error) {
      console.log(error)
    })    
  }

  function handleDone(){
    setIsTicking(false);
    setIsFront(false);
  }

  return (
    <div className="tiletext">
      {
        isFront 
        ?
        <Descriptions title="Front">
          <Descriptions.Item>{props.front}</Descriptions.Item>
        </Descriptions>
        :
        <Descriptions title="Back">
          <Descriptions.Item>{props.back}</Descriptions.Item>
        </Descriptions>
      }

      {
        isFront 
        ?
        <div>
          <Button className="Button-Create" type="primary" onClick={() => handleDone()}>Done</Button>
        </div>
        :
        <div>
          <Button 
            danger 
            size="small"
            type="primary" 
            onClick={() => handleClick(0, props.recallId)}>
              Again
          </Button>

          <Button
            className="hard-button" 
            size="small"
            onClick={() => handleClick(1, props.recallId)}>
              Hard
          </Button>

          <Button
            type="primary" 
            size="small"
            onClick={() => handleClick(2, props.recallId)}>
              Good
          </Button>

          <Button 
            className="easy-button" 
            size="small"
            onClick={() => handleClick(3, props.recallId)}>
              Easy
          </Button>

        </div>
      }
    </div>
  )
}
  
function RecallsPage(props) {

  const [deck, setDeck] = useState({});
  const [recalls, setRecalls] = useState([]);
  const [rIndex, setrIndex] = useState(0);

  useEffect(() => {
    if (props.page.targetId !== 0) { 
      DeckService.get(props.page.targetId)
        .then(function (response) {
          setDeck(response);
        })
        .catch(function (error) {
          console.log(error);
        })
    }
  }, [props.page.targetId]); 

  useEffect(() => {
    if (props.user_id !== 'None') { 
      RecallService.list(props.page.targetId)
      .then(function (response) {
        setRecalls(response);
      })
      .catch(function (error) {
        console.log(error);
      })
    }
  }, [props.page.targetId]);  

  const deckTitle = `Practice Deck: ${deck.title}`;
  const headerDiv= 
    <div className="Deck-Item">
      <div>
        {deckTitle}
      </div>
      <div className="Deck-Icons">
        <HomeOutlined onClick={() => props.pageHandler('decks', 0)}/>
      </div>
    </div>;

  return (

    <Card type="inner" title={headerDiv} className="Card-Inner" >

      {
        recalls.map(function(recall, idx){
          if (idx === rIndex) {
              return <RecallTile 
                key={idx}
                front={recall.card_front}
                back={recall.card_back}
                recallId={recall.id}
                setrIndex={setrIndex}
                rIndex={rIndex}
              />
          }
          else {
            return null;
          }
        })
      }
      
      { 
          rIndex === recalls.length 
          &&
          "Done for the day!"
      }

    </Card>
  )
  
}
  
export default RecallsPage;  