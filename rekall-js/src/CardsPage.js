import CardService from './api/cardservice';
import DeckService from './api/deckservice';

import { React, useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { Card, Button, List } from 'antd';
import { CloseOutlined, EditOutlined, HomeOutlined } from '@ant-design/icons';


function CardTile(props) {

  const [editCard, setEditCard] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();

  function handleClick(val){
    setEditCard(val);
  }

  function wrapSubmit(input){
    props.onSubmitEditCard(input);
    setEditCard(!editCard);
  }

  return (
    <div className="tiletext">
      {
        editCard 
        ?
        <div>
          <form onSubmit={handleSubmit(wrapSubmit)}>
            <input defaultValue={props.front} {...register("front", { required: true })} />
            <input defaultValue={props.back} {...register("back", { required: true })} />
            <input hidden={true} defaultValue={props.card_id} {...register("id", { required: true })} />

            {errors.front && <span>The front is required</span>}
            {errors.back && <span>The back is required</span>}
    
            <input type="submit" />
            <CloseOutlined onClick={() => handleClick(false)} />
          </form>
        </div>      
        :
        <div className="Deck-Item">
          <div className="Card-Parts"><div>
            Front: {props.front} 
          </div>
          <div>
            Back: {props.back}
          </div> </div>
          <div className="Deck-Icons">
            <EditOutlined onClick={() => handleClick(true)}/>
          </div>
        </div>
      }
    </div>
  )
}


function CardsPage(props) {

  const [deck, setDeck] = useState({});
  const [cards, setCards] = useState([]);
  const [editDeck, setEditDeck] = useState(false);
  const [addCard, setAddCard] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();

  const onSubmitEditDeck = data => {
    data['owner'] = props.user_id;
    data['creator'] = props.user_id;
    DeckService.update(deck.id, data)
    .then(function (response) {
      setDeck(response);
      setEditDeck(false);
    })
    .catch(function (error) {
     console.log(error);
    }) 
  };
  
  function cardUpdate(response) {
    const itemIndex = cards.findIndex((elem) => elem.id === response.id);
    if (itemIndex === -1) {
      return;
    }
    const newCards = [...cards];
    newCards[itemIndex] = response;
    setCards([...newCards]);
  }  

  const onSubmitEditCard = data => {
    data['owner'] = props.user_id;
    data['creator'] = props.user_id;
    data['deck'] = deck.id;

    CardService.update(data['id'], data)
    .then(function (response) {
      console.log('in then');
      cardUpdate(response);
    })
    .catch(function (error) {
     console.log(error);
    }) 
  };

  const onSubmitNewCard = data => {
    data['owner'] = props.user_id;
    data['creator'] = props.user_id;
    data['deck'] = props.page.targetId;

    CardService.create(data)
    .then(function (response) {
      setCards(cards => [...cards, response]);
      setAddCard(false);
    })
    .catch(function (error) {
      console.log(error);
    }) 
  };

  useEffect(() => {
    if (props.page.targetId !== 0) { 
      DeckService.get(props.page.targetId)
      .then(function (response) {
        setDeck(response);
      })
      .catch(function (error) {
        console.log(error);
      })
    }
  }, [props.page.targetId]); 

  useEffect(() => {
    if (props.page.targetId !== 0) { 
      CardService.list(props.page.targetId)
      .then(function (response) {
        setCards(response);
      })
      .catch(function (error) {
        console.log(error);
      })
    }
  }, [props.page.targetId]);

  const deckTitle = `Deck: ${deck.title}`;
  const headerDiv= 
    <div className="Deck-Item">
      {
        editDeck 
        && props.user_id !== 'None'
        ?
        <form onSubmit={handleSubmit(onSubmitEditDeck)}>
          <input defaultValue={deck.title} {...register("title", { required: true })} />        
          {errors.title && <span>The title is required</span>}
          <input type="submit" />
        </form>
        :
        <div>
          {deckTitle}
        </div>
      }

      <div className="Deck-Icons">
        {
          editDeck 
          ? <CloseOutlined onClick={() => setEditDeck(false)} />
          : <EditOutlined onClick={() => setEditDeck(true)}/>
        }
        <HomeOutlined onClick={() => props.pageHandler('decks', 0)}/>
      </div>
    </div>;

  return (
    <Card type="inner" title={headerDiv} className="Card-Inner" >

      {
        !addCard 
        && <Button className="Button-Create" type="primary" onClick={() => setAddCard(true)}>Create Card</Button>
      }

      {
        addCard 
        && props.user_id !== 'None'
        && 
        <form className="Form-New-Card" onSubmit={handleSubmit(onSubmitNewCard)}>
          <input defaultValue="front" {...register("front", { required: true })} />        
          <input defaultValue="back" {...register("back", { required: true })} />

          {errors.front && <span>The front is required</span>}
          {errors.back && <span>The back is required</span>}

          <div>
            <input type="submit" />
            <CloseOutlined onClick={() => setAddCard(false)} />
          </div>
        </form>
      }      
    
      { 
        cards.length > 0 
        && 
        <List
          bordered
          dataSource={cards}
          renderItem={card => (
            <List.Item >
              <CardTile 
                key={card.id} 
                front={card.front}
                back={card.back} 
                user_id={props.user_id} 
                onSubmitEditCard={onSubmitEditCard}
                card_id={card.id}
                deck_id={deck.id} />
            </List.Item>
          )} />
      }    

    </Card>
  );
  }

export default CardsPage;