import DeckService from './api/deckservice';

import { useState, useEffect, React } from 'react';
import { useForm } from "react-hook-form";
import { Card, Button, List } from 'antd';
import { CloseOutlined, EditOutlined, DoubleRightOutlined } from '@ant-design/icons';


function DeckTile(props) {

  function handleClick(page, deck_id){
    props.setPage({
      page: page,
      targetId: deck_id
    })
  }

  return (
    <div className="Deck-Item">
      <div>
        {props.text}
      </div>
      <div className="Deck-Icons">
        <div>
          <EditOutlined onClick={() => handleClick('cards', props.deck_id)}/>
        </div>
        <div>
          <DoubleRightOutlined onClick={() => handleClick('recalls', props.deck_id)} />
        </div>
      </div>
    </div>
  )
}
  
function DecksPage(props) {

  const [decks, setDecks] = useState([]);
  const [addTile, setAddTile] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();  

  const onSubmit = data => {
    data['owner'] = props.user_id;
    data['creator'] = props.user_id;

    DeckService.create(data)
    .then(function (response) {
      setDecks(decks => [...decks, response]);
    })
    .catch(function (error) {
      console.log(error);
    }) 
  };  

  useEffect(() => {
    if (props.user_id !== 'None') { 
      DeckService.list()
      .then(function (response) {
        setDecks(response);
      })
      .catch(function (error) {
        console.log(error);
      })
    }
  }, [props.user_id]);  

  return (
    <Card type="inner" title="Decks" className="Card-Inner" >      
      {
        !addTile 
        && 
        <Button className="Button-Create" type="primary" onClick={() => setAddTile(true)}>Create Deck</Button>
      } 

      {
        addTile 
        && props.user_id !== 'None'
        && 
        <form onSubmit={handleSubmit(onSubmit)}>
          <input defaultValue="test" {...register("title", { required: true })} />        
          {errors.title && <span>This field is required</span>}        
          <input type="submit" />
          <CloseOutlined onClick={() => setAddTile(false)} />
        </form>
      }  

      { 
        decks.length > 0 
        && 
        <List
          bordered
          dataSource={decks}
          renderItem={item => (
            <List.Item>
              <DeckTile 
                text={item.title} 
                setPage={props.setPage} 
                user_id={props.user_id} 
                deck_id={item.id} />
            </List.Item>
          )} />
      }   
    </Card>
  )  
}
  
export default DecksPage;  