import './App.css';
import CardsPage from './CardsPage';
import DecksPage from './DecksPage';
import RecallsPage from './RecallsPage';

import { useState, React } from 'react';
import { Card } from 'antd';


function App(props) {

  const [page, setPage] = useState({page: "decks", targetId: 0});

  function pageHandler(page, targetId) {
    setPage({
      page: page,
      targetId: targetId
    })
  }

  const route = {
    page, setPage, pageHandler
  }

  return(
    <Card className="App" title="Rekall">
      { (page.page === "decks") && <DecksPage {...route} {...props} /> }
      { (page.page === "cards") && <CardsPage {...route} {...props} /> }
      { (page.page === "recalls") && <RecallsPage {...route} {...props} /> }
    </Card>
  )
}

export default App;
