import { useState, useEffect } from "react";

export const TimerComponent = () => {

  const [isTicking, setIsTicking] = useState(false);
  const [seconds, setSeconds] = useState(0);

  useEffect(
    () => {
      let interval;
      if (isTicking) {
        interval = setInterval(
          () => setSeconds(prevSeconds => prevSeconds + 0.1), 100
        );
      }
      return () => clearInterval(interval);
    },
    [isTicking]
  );

  return {
    isTicking,
    setIsTicking,
    seconds,
    setSeconds
  };
};
