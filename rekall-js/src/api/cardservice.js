import request from '../utils/requests';

function get(id) {
  return request({
    url:    `/api/cards/${id}`,
    method: 'GET'
  });
}

function list(id) {
  return request({
    url:    `/api/cards/deck/${id}`,
    method: 'GET'
  });
}

function create({owner, creator, front, back, deck}) {
  return request({
    url:    '/api/cards/',
    method: 'POST',
    data:  {owner, creator, front, back, deck}
  });
}

function update(id, {owner, creator, front, back, deck}) {
  return request({
    url:    `/api/cards/${id}`,
    method: 'PUT',
    data:  {owner, creator, front, back, deck}
  });
}

function remove(id) {
  return request({
    url:    `/api/cards/${id}`,
    method: 'DELETE'
  });
}
  
const CardService = {
  get, create, update, remove, list
}
  
export default CardService;
  