import request from '../utils/requests';

function list(id) {
  return request({
    url:    `/api/recalls/deck/${id}`,
    method: 'GET'
  });
}

function update(id, {score, duration}) {
  return request({
    url:    `/api/recalls/${id}`,
    method: 'PATCH',
    data:  {score, duration}
  });
}
  
const RecallService = {
  update, list
}
  
export default RecallService;
  