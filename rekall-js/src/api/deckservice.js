import request from '../utils/requests';

function get(id) {
  return request({
    url:    `/api/decks/${id}`,
    method: 'GET'
  });
}

function list() {
  return request({
    url:    `/api/decks/`,
    method: 'GET'
  });
}

function create({owner, creator, title}) {
  return request({
    url:    '/api/decks/',
    method: 'POST',
    data:  {owner, creator, title}
  });
}

function update(id, {owner, creator, title}) {
  return request({
    url:    `/api/decks/${id}`,
    method: 'PUT',
    data:  {owner, creator, title}
  });
}

function remove(id) {
  return request({
    url:    `/api/decks/${id}`,
    method: 'DELETE'
  });
}
  
const DeckService = {
  get, create, update, remove, list
}
  
export default DeckService;
  