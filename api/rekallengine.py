import datetime

from .models import Card, Deck, Recall


class RekallEngine:
    t23h_ago = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(hours=23)
    default_timediff = t23h_ago

    def __init__(self, deck_id, user):
        if type(deck_id) != int:
            raise TypeError("deck id not of type int")
        self.deck_id = deck_id
        self.user = user

    def retrieve_deck(self):
        return Deck.objects.filter(id=self.deck_id).first()

    def retrieve_cards(self):
        return Card.objects.all().filter(owner=self.user).filter(deck=self.deck_id)

    def create_recalls(self, new_recalls):
        Recall.objects.bulk_create([ Recall(**obj_fields) for obj_fields in new_recalls ])

    def retrieve_recalls(self):
        return Recall.objects.filter(owner=self.user, deck=self.deck_id, created__gt=self.default_timediff)

    def count_recalls(self):
        return Recall.objects.filter(owner=self.user, deck=self.deck_id, created__gt=self.default_timediff).count()

    def generate(self):
        count = self.count_recalls()
        deck = self.retrieve_deck()
        if count == 0:
            target_cards = self.retrieve_cards()
            new_recalls = []
            for target_card in target_cards:
                new_recall = {}
                new_recall['card'] = target_card
                new_recall['deck'] = deck
                new_recall['owner'] = self.user
                new_recalls.append(new_recall)
            self.create_recalls(new_recalls)
        # return self.retrieve_recalls()
