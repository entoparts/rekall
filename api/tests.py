import json

from django.test import TestCase
from django.db.utils import IntegrityError
from rest_framework.test import APIClient

from .models import Card, Deck, Recall
from someapp.models import User


class APIClientTests(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='some', email='some@some', password='pass')
        self.client = APIClient()

    def test_base_user_login(self):
        res = self.client.login(username='some', password='notsecret')
        self.assertIs(res, False)
        res = self.client.login(username='some', password='pass')
        self.assertIs(res, True)

    def test_get_decks(self):
        res = self.client.login(username='some', password='pass')
        self.assertIs(res, True)
        response = self.client.get('/api/decks/')
        self.assertEqual(response.status_code, 200)

    def test_base_deck_post(self):
        res = self.client.login(username='some', password='pass')
        response = self.client.post(path='/api/decks/', 
            data=json.dumps({'title': 'new idea', 'owner': self.user.id, 'creator': self.user.id}), 
            content_type='application/json')
        self.assertEquals(response.status_code, 201)
        dd = Deck.objects.get(title='new idea')
        with self.assertRaises(Deck.DoesNotExist):
            d2 = Deck.objects.get(title='old idea')

    def test_base_deck_post_missing_owner(self):
        res = self.client.login(username='some', password='pass')
        response = self.client.post(path='/api/decks/', 
            data=json.dumps({'title': 'new idea', 'creator': self.user.id}), 
            content_type='application/json')
        self.assertEquals(response.status_code, 400)

class DeckModelTests(TestCase):

    def test_deck_as_expected(self):
        some_user = User()
        some_user.save()
        some_deck = Deck()
        some_deck.title = 'asd'
        some_deck.owner = some_user
        some_deck.creator = some_user
        some_deck.save()

    def test_deck_no_creator(self):
        some_user = User()
        some_user.save()
        some_deck = Deck()
        some_deck.owner = some_user
        with self.assertRaises(IntegrityError):
            some_deck.save()