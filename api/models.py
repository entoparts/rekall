from django.db import models
from django.conf import settings
from mainapp.models import User


class Deck(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    creator = models.ForeignKey(User, on_delete=models.RESTRICT, related_name="deck_creater_user")
    owner = models.ForeignKey(User, on_delete=models.RESTRICT, related_name="deck_owner_user")
    
    class Meta:
        ordering = ['created']

class Card(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    front = models.CharField(max_length=100, blank=True, default='')
    back = models.CharField(max_length=100, blank=True, default='')
    creator = models.ForeignKey(User, on_delete=models.RESTRICT, related_name="card_creator_user")
    owner = models.ForeignKey(User, on_delete=models.RESTRICT, related_name="card_owner_user")
    deck = models.ForeignKey(Deck, on_delete=models.RESTRICT)
    
    class Meta:
        ordering = ['created']

class Recall(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    card = models.ForeignKey(Card, on_delete=models.RESTRICT)
    deck = models.ForeignKey(Deck, on_delete=models.RESTRICT)
    owner = models.ForeignKey(User, on_delete=models.RESTRICT, related_name="recall_owner_user")
    score = models.IntegerField(default=None, null=True) # 0-3
    remembered = models.BooleanField(default=None, null=True)
    duration = models.DecimalField(default=None, null=True, max_digits=12, decimal_places=1)

    class Meta:
        ordering = ['created']


