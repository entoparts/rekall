from rest_framework import serializers
from mainapp.models import User
from api.models import Deck, Card, Recall


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email']


class DeckSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source="owner.username", read_only=True)

    class Meta:
        model = Deck
        fields = ['created', 'title', 'owner', 'creator', 'id', 'username']


class CardSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source="owner.username", read_only=True)
    deck_title = serializers.CharField(source="deck.title", read_only=True)

    class Meta:
        model = Card
        fields = ['created', 'deck', 'creator', 'owner', 'id', 'front', 'back', 'username', 'deck_title']

class RecallSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source="owner.username", read_only=True)
    deck_title = serializers.CharField(source="deck.title", read_only=True)
    card_front = serializers.CharField(source="card.front", read_only=True)
    card_back = serializers.CharField(source="card.back", read_only=True)

    class Meta:
        model = Recall
        fields = ['card', 'owner', 'deck', 'id', 'created', 'score', 'remembered', 'duration', 'deck_title', 'card_front', 'card_back', 'username']