from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from django.shortcuts import render

from api import views
from rest_framework import routers


urlpatterns = [
    path('cards/<int:pk>', views.CardDetail.as_view()),
    path('cards/', views.CardList.as_view()),
    path('cards/deck/<int:deck_id>', views.CardDeckList.as_view()),
    path('recalls/deck/<int:deck_id>', views.RecallDeckList.as_view()),
    path('recalls/<int:pk>', views.RecallDetail.as_view()),
    path('decks/<int:pk>', views.DeckDetail.as_view()),
    path('decks/', views.DeckList.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

