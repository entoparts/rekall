import datetime

from mainapp.models import User
from api.models import Deck, Card, Recall
from api.serializers import UserSerializer, DeckSerializer, CardSerializer, RecallSerializer
from api.rekallengine import RekallEngine

from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

class CardDeckList(generics.ListAPIView):
    serializer_class = CardSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        deck_id = self.kwargs.get('deck_id', None)
        return Card.objects.filter(owner=self.request.user, deck=deck_id)

class CardList(generics.ListCreateAPIView):
    serializer_class = CardSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Card.objects.all().filter(owner=self.request.user)


class RecallDeckList(generics.ListAPIView):
    serializer_class = RecallSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        deck_id = self.kwargs.get('deck_id', None)
        t23h_ago = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(hours=23)
        rek = RekallEngine(deck_id=deck_id, user=self.request.user)
        rek.generate()            
        return Recall.objects.filter(owner=self.request.user, deck=deck_id, created__gt=t23h_ago, score__isnull=True)

class RecallDetail(generics.RetrieveUpdateAPIView):
    serializer_class = RecallSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Recall.objects.filter(owner=self.request.user)

    def patch(self, request, pk, format=None):
        recall_obj = Recall.objects.get(pk=pk)
        print(request.data)
        request.data['remembered'] = True
        if request.data['score'] == 0:
            request.data['remembered'] = False
        serializer = RecallSerializer(recall_obj, data=request.data, partial=True) # set partial=True to update a data partially
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CardDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CardSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Card.objects.filter(owner=self.request.user)


class DeckList(generics.ListCreateAPIView):
    serializer_class = DeckSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Deck.objects.filter(owner=self.request.user)


class DeckDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DeckSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Deck.objects.filter(owner=self.request.user)