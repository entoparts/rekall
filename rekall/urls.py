from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from rest_framework import routers
from cra_helper.views import proxy_cra_requests

@login_required
def index(request):
    user_id = None
    if request.user.id:
        user_id = int(request.user.id)
    context = {
        'component': 'App',
        'props': {
            'env': 'Django',
            'user_id': str(user_id)
        },
    }
    return render(request, 'index.html', context)

urlpatterns = [
    path('', index, name='index'),
    path('accounts/', include('mainapp.urls')),
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
]

if settings.DEBUG:
    proxy_urls = [
        re_path(r'^__webpack_dev_server__/(?P<path>.*)$', proxy_cra_requests),
        re_path(r'^(?P<path>.+\.hot-update\.(js|json|js\.map))$', proxy_cra_requests),
    ]
    urlpatterns.extend(proxy_urls)
